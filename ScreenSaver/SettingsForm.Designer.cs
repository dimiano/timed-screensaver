﻿namespace TimedScreenSaver
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.contentTextBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.okButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.contentDisplayGroupBox = new System.Windows.Forms.GroupBox();
            this.colorBtn = new System.Windows.Forms.Button();
            this.fontBtn = new System.Windows.Forms.Button();
            this.timePatternGroupBox = new System.Windows.Forms.GroupBox();
            this.secondsCheckBox = new System.Windows.Forms.CheckBox();
            this.yearCheckBox = new System.Windows.Forms.CheckBox();
            this.dateCheckBox = new System.Windows.Forms.CheckBox();
            this.textRadioBtn = new System.Windows.Forms.RadioButton();
            this.timeRadioBtn = new System.Windows.Forms.RadioButton();
            this.colorGroupBox = new System.Windows.Forms.GroupBox();
            this.backColorPanel = new System.Windows.Forms.Panel();
            this.dateLbl = new System.Windows.Forms.Label();
            this.contentLbl = new System.Windows.Forms.Label();
            this.positionGroupBox = new System.Windows.Forms.GroupBox();
            this.customPositionBtn = new System.Windows.Forms.Button();
            this.customPositionLbl = new System.Windows.Forms.Label();
            this.positionPanel = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.customPositionRadioBtn = new System.Windows.Forms.RadioButton();
            this.definedPositionRadioBtn = new System.Windows.Forms.RadioButton();
            this.colorDialog = new System.Windows.Forms.ColorDialog();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.fontDialog = new System.Windows.Forms.FontDialog();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.contentDisplayGroupBox.SuspendLayout();
            this.timePatternGroupBox.SuspendLayout();
            this.colorGroupBox.SuspendLayout();
            this.backColorPanel.SuspendLayout();
            this.positionGroupBox.SuspendLayout();
            this.positionPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // contentTextBox
            // 
            resources.ApplyResources(this.contentTextBox, "contentTextBox");
            this.contentTextBox.Name = "contentTextBox";
            this.contentTextBox.TextChanged += new System.EventHandler(this.ContentTextBoxTextChanged);
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.okButton, "okButton");
            this.okButton.Name = "okButton";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.OkButtonClick);
            // 
            // cancelButton
            // 
            this.cancelButton.CausesValidation = false;
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.cancelButton, "cancelButton");
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.CancelButtonClick);
            // 
            // tableLayoutPanel1
            // 
            resources.ApplyResources(this.tableLayoutPanel1, "tableLayoutPanel1");
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.contentDisplayGroupBox, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.colorGroupBox, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.positionGroupBox, 0, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            // 
            // tableLayoutPanel2
            // 
            resources.ApplyResources(this.tableLayoutPanel2, "tableLayoutPanel2");
            this.tableLayoutPanel2.Controls.Add(this.cancelButton, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.okButton, 0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            // 
            // contentDisplayGroupBox
            // 
            this.contentDisplayGroupBox.Controls.Add(this.colorBtn);
            this.contentDisplayGroupBox.Controls.Add(this.fontBtn);
            this.contentDisplayGroupBox.Controls.Add(this.timePatternGroupBox);
            this.contentDisplayGroupBox.Controls.Add(this.contentTextBox);
            this.contentDisplayGroupBox.Controls.Add(this.textRadioBtn);
            this.contentDisplayGroupBox.Controls.Add(this.timeRadioBtn);
            resources.ApplyResources(this.contentDisplayGroupBox, "contentDisplayGroupBox");
            this.contentDisplayGroupBox.Name = "contentDisplayGroupBox";
            this.contentDisplayGroupBox.TabStop = false;
            // 
            // colorBtn
            // 
            resources.ApplyResources(this.colorBtn, "colorBtn");
            this.colorBtn.Name = "colorBtn";
            this.colorBtn.UseVisualStyleBackColor = true;
            this.colorBtn.Click += new System.EventHandler(this.ColorBtnClick);
            // 
            // fontBtn
            // 
            resources.ApplyResources(this.fontBtn, "fontBtn");
            this.fontBtn.Name = "fontBtn";
            this.fontBtn.UseVisualStyleBackColor = true;
            this.fontBtn.Click += new System.EventHandler(this.FontBtnClick);
            // 
            // timePatternGroupBox
            // 
            this.timePatternGroupBox.Controls.Add(this.secondsCheckBox);
            this.timePatternGroupBox.Controls.Add(this.yearCheckBox);
            this.timePatternGroupBox.Controls.Add(this.dateCheckBox);
            resources.ApplyResources(this.timePatternGroupBox, "timePatternGroupBox");
            this.timePatternGroupBox.Name = "timePatternGroupBox";
            this.timePatternGroupBox.TabStop = false;
            // 
            // secondsCheckBox
            // 
            resources.ApplyResources(this.secondsCheckBox, "secondsCheckBox");
            this.secondsCheckBox.Name = "secondsCheckBox";
            this.secondsCheckBox.UseVisualStyleBackColor = true;
            this.secondsCheckBox.CheckedChanged += new System.EventHandler(this.TimePatternChanged);
            // 
            // yearCheckBox
            // 
            resources.ApplyResources(this.yearCheckBox, "yearCheckBox");
            this.yearCheckBox.Name = "yearCheckBox";
            this.yearCheckBox.UseVisualStyleBackColor = true;
            this.yearCheckBox.CheckedChanged += new System.EventHandler(this.YearCheckBoxCheckedChanged);
            // 
            // dateCheckBox
            // 
            resources.ApplyResources(this.dateCheckBox, "dateCheckBox");
            this.dateCheckBox.Name = "dateCheckBox";
            this.dateCheckBox.UseVisualStyleBackColor = true;
            this.dateCheckBox.CheckedChanged += new System.EventHandler(this.DateCheckBoxCheckedChanged);
            // 
            // textRadioBtn
            // 
            resources.ApplyResources(this.textRadioBtn, "textRadioBtn");
            this.textRadioBtn.Name = "textRadioBtn";
            this.textRadioBtn.TabStop = true;
            this.textRadioBtn.UseVisualStyleBackColor = true;
            this.textRadioBtn.CheckedChanged += new System.EventHandler(this.ContentTypeChanged);
            // 
            // timeRadioBtn
            // 
            resources.ApplyResources(this.timeRadioBtn, "timeRadioBtn");
            this.timeRadioBtn.Checked = true;
            this.timeRadioBtn.Name = "timeRadioBtn";
            this.timeRadioBtn.TabStop = true;
            this.timeRadioBtn.UseVisualStyleBackColor = true;
            this.timeRadioBtn.CheckedChanged += new System.EventHandler(this.ContentTypeChanged);
            // 
            // colorGroupBox
            // 
            this.colorGroupBox.BackColor = System.Drawing.SystemColors.Control;
            this.colorGroupBox.Controls.Add(this.backColorPanel);
            resources.ApplyResources(this.colorGroupBox, "colorGroupBox");
            this.colorGroupBox.ForeColor = System.Drawing.SystemColors.ControlText;
            this.colorGroupBox.Name = "colorGroupBox";
            this.colorGroupBox.TabStop = false;
            // 
            // backColorPanel
            // 
            this.backColorPanel.BackColor = System.Drawing.Color.Black;
            this.backColorPanel.Controls.Add(this.dateLbl);
            this.backColorPanel.Controls.Add(this.contentLbl);
            resources.ApplyResources(this.backColorPanel, "backColorPanel");
            this.backColorPanel.Name = "backColorPanel";
            // 
            // dateLbl
            // 
            resources.ApplyResources(this.dateLbl, "dateLbl");
            this.dateLbl.BackColor = System.Drawing.Color.Black;
            this.dateLbl.ForeColor = System.Drawing.Color.Green;
            this.dateLbl.Name = "dateLbl";
            // 
            // contentLbl
            // 
            resources.ApplyResources(this.contentLbl, "contentLbl");
            this.contentLbl.BackColor = System.Drawing.Color.Black;
            this.contentLbl.ForeColor = System.Drawing.Color.Green;
            this.contentLbl.Name = "contentLbl";
            // 
            // positionGroupBox
            // 
            this.positionGroupBox.Controls.Add(this.customPositionBtn);
            this.positionGroupBox.Controls.Add(this.customPositionLbl);
            this.positionGroupBox.Controls.Add(this.positionPanel);
            this.positionGroupBox.Controls.Add(this.customPositionRadioBtn);
            this.positionGroupBox.Controls.Add(this.definedPositionRadioBtn);
            resources.ApplyResources(this.positionGroupBox, "positionGroupBox");
            this.positionGroupBox.Name = "positionGroupBox";
            this.positionGroupBox.TabStop = false;
            // 
            // customPositionBtn
            // 
            resources.ApplyResources(this.customPositionBtn, "customPositionBtn");
            this.customPositionBtn.Name = "customPositionBtn";
            this.customPositionBtn.UseVisualStyleBackColor = true;
            this.customPositionBtn.Click += new System.EventHandler(this.CustomPositionBtnClick);
            // 
            // customPositionLbl
            // 
            resources.ApplyResources(this.customPositionLbl, "customPositionLbl");
            this.customPositionLbl.Name = "customPositionLbl";
            // 
            // positionPanel
            // 
            this.positionPanel.BackColor = System.Drawing.Color.White;
            this.positionPanel.Controls.Add(this.button6);
            this.positionPanel.Controls.Add(this.button5);
            this.positionPanel.Controls.Add(this.button4);
            this.positionPanel.Controls.Add(this.button9);
            this.positionPanel.Controls.Add(this.button3);
            this.positionPanel.Controls.Add(this.button8);
            this.positionPanel.Controls.Add(this.button2);
            this.positionPanel.Controls.Add(this.button7);
            this.positionPanel.Controls.Add(this.button1);
            resources.ApplyResources(this.positionPanel, "positionPanel");
            this.positionPanel.Name = "positionPanel";
            // 
            // button6
            // 
            resources.ApplyResources(this.button6, "button6");
            this.button6.Name = "button6";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.PositionButtonClick);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Green;
            resources.ApplyResources(this.button5, "button5");
            this.button5.Name = "button5";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.PositionButtonClick);
            // 
            // button4
            // 
            resources.ApplyResources(this.button4, "button4");
            this.button4.Name = "button4";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.PositionButtonClick);
            // 
            // button9
            // 
            resources.ApplyResources(this.button9, "button9");
            this.button9.Name = "button9";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.PositionButtonClick);
            // 
            // button3
            // 
            resources.ApplyResources(this.button3, "button3");
            this.button3.Name = "button3";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.PositionButtonClick);
            // 
            // button8
            // 
            resources.ApplyResources(this.button8, "button8");
            this.button8.Name = "button8";
            this.button8.UseVisualStyleBackColor = false;
            this.button8.Click += new System.EventHandler(this.PositionButtonClick);
            // 
            // button2
            // 
            resources.ApplyResources(this.button2, "button2");
            this.button2.Name = "button2";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.PositionButtonClick);
            // 
            // button7
            // 
            resources.ApplyResources(this.button7, "button7");
            this.button7.Name = "button7";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.PositionButtonClick);
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.PositionButtonClick);
            // 
            // customPositionRadioBtn
            // 
            resources.ApplyResources(this.customPositionRadioBtn, "customPositionRadioBtn");
            this.customPositionRadioBtn.Name = "customPositionRadioBtn";
            this.customPositionRadioBtn.TabStop = true;
            this.customPositionRadioBtn.UseVisualStyleBackColor = true;
            this.customPositionRadioBtn.CheckedChanged += new System.EventHandler(this.CustomPositionRadioBtnCheckedChanged);
            // 
            // definedPositionRadioBtn
            // 
            resources.ApplyResources(this.definedPositionRadioBtn, "definedPositionRadioBtn");
            this.definedPositionRadioBtn.Checked = true;
            this.definedPositionRadioBtn.Name = "definedPositionRadioBtn";
            this.definedPositionRadioBtn.TabStop = true;
            this.definedPositionRadioBtn.UseVisualStyleBackColor = true;
            this.definedPositionRadioBtn.CheckedChanged += new System.EventHandler(this.DefinedPositionRadioBtnCheckedChanged);
            // 
            // timer
            // 
            this.timer.Interval = 1000;
            this.timer.Tick += new System.EventHandler(this.TimerTick);
            // 
            // SettingsForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.tableLayoutPanel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsFormFormClosing);
            this.Load += new System.EventHandler(this.SettingsFormLoad);
            this.SizeChanged += new System.EventHandler(this.SettingsFormSizeChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.contentDisplayGroupBox.ResumeLayout(false);
            this.contentDisplayGroupBox.PerformLayout();
            this.timePatternGroupBox.ResumeLayout(false);
            this.timePatternGroupBox.PerformLayout();
            this.colorGroupBox.ResumeLayout(false);
            this.backColorPanel.ResumeLayout(false);
            this.backColorPanel.PerformLayout();
            this.positionGroupBox.ResumeLayout(false);
            this.positionGroupBox.PerformLayout();
            this.positionPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox contentTextBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.GroupBox contentDisplayGroupBox;
        private System.Windows.Forms.RadioButton timeRadioBtn;
        private System.Windows.Forms.RadioButton textRadioBtn;
        private System.Windows.Forms.ColorDialog colorDialog;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.GroupBox colorGroupBox;
        private System.Windows.Forms.Label contentLbl;
        private System.Windows.Forms.Panel backColorPanel;
        private System.Windows.Forms.FontDialog fontDialog;
        private System.Windows.Forms.GroupBox positionGroupBox;
        private System.Windows.Forms.RadioButton customPositionRadioBtn;
        private System.Windows.Forms.RadioButton definedPositionRadioBtn;
        private System.Windows.Forms.Label customPositionLbl;
        private System.Windows.Forms.Panel positionPanel;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox timePatternGroupBox;
        private System.Windows.Forms.Button fontBtn;
        private System.Windows.Forms.Button colorBtn;
        private System.Windows.Forms.CheckBox dateCheckBox;
        private System.Windows.Forms.Label dateLbl;
        private System.Windows.Forms.Button customPositionBtn;
        private System.Windows.Forms.CheckBox secondsCheckBox;
        private System.Windows.Forms.CheckBox yearCheckBox;
    }
}