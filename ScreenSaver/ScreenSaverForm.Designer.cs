﻿namespace TimedScreenSaver
{
    partial class ScreenSaverForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.screenTimer = new System.Windows.Forms.Timer(this.components);
            this.contentLbl = new System.Windows.Forms.Label();
            this.dateLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // screenTimer
            // 
            this.screenTimer.Interval = 1000;
            this.screenTimer.Tick += new System.EventHandler(this.TimerTick);
            // 
            // contentLbl
            // 
            this.contentLbl.AutoSize = true;
            this.contentLbl.Font = new System.Drawing.Font("Arial", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.contentLbl.ForeColor = System.Drawing.Color.Green;
            this.contentLbl.Location = new System.Drawing.Point(167, 94);
            this.contentLbl.Name = "contentLbl";
            this.contentLbl.Size = new System.Drawing.Size(180, 55);
            this.contentLbl.TabIndex = 0;
            this.contentLbl.Text = "Locked";
            this.contentLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.contentLbl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ContentLblMouseDown);
            this.contentLbl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ContentLblMouseMove);
            this.contentLbl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ContentLblMouseUp);
            // 
            // dateLbl
            // 
            this.dateLbl.AutoSize = true;
            this.dateLbl.Font = new System.Drawing.Font("Arial", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateLbl.ForeColor = System.Drawing.Color.Green;
            this.dateLbl.Location = new System.Drawing.Point(175, 149);
            this.dateLbl.Name = "dateLbl";
            this.dateLbl.Size = new System.Drawing.Size(161, 22);
            this.dateLbl.TabIndex = 0;
            this.dateLbl.Text = "Sunday | 5 of May";
            this.dateLbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.dateLbl.Visible = false;
            this.dateLbl.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ContentLblMouseDown);
            this.dateLbl.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ContentLblMouseMove);
            this.dateLbl.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ContentLblMouseUp);
            // 
            // ScreenSaverForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(508, 260);
            this.Controls.Add(this.dateLbl);
            this.Controls.Add(this.contentLbl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "ScreenSaverForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "Timed ScreenSaver";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.ScreenSaverFormLoad);
            this.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ScreenSaverFormKeyPress);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ScreenSaverFormMouseClick);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ScreenSaverFormMouseMove);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Timer screenTimer;
        private System.Windows.Forms.Label contentLbl;
        private System.Windows.Forms.Label dateLbl;
    }
}

