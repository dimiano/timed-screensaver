# Timed ScreenSaver #

is light screen saver with whatever you want text, time and/or date on it.
Timed ScreenSaver is light screen saver with whatever you want text, time and/or date on it.
You can set text/time/date on the screen saver wherever you want.

Support **English and Russian** languages (Ru *with separate file*).

1. You can set text/time/date on the screen saver wherever you want:

   * By selecting predefined positions,
   * By selecting text position you like by mouse drug&drop.

1. Also you can select font, font color and background color to any you like.
1. You can type text to display on screensaver.
1. And of course you can modify source code for your purposes.
1. Version 1.2: optional year displaying now!

## How to install Screensaver: ##

Just google it: How to install screensaver

**Windows XP**:

1. Copy the SCR file in your system folder (if you have administrative privileges on your computer) into **C:\windows\system32** or **C:\windows\SysWOW64** if you are running a 64-bit version of Windows.
1. Right-click in the Windows Desktop background and select "Properties". A dialog box opens. Select the "Screen Saver" tab
1. Select your screensaver in the list. (it should appear in this list if you copied it in the correct folder). The screensaver appears in the Preview screensource.

**Vista/7/8/10**:

Right-click on Timed ScreenSaver.**scr** and select **Install** from the pop-up menu. This will launch the screen saver setting dialog with **Timed ScreenSaver** selectedsource.
You can adjust the screensaver settings or display it fullscreen
When all done, click OK.